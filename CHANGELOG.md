# CHANGELOG

All notable changes to this project are documented in this file.

This changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- 
## [0.0.5](https://gitlab.com/envelop/mintnft/mintfactory/-/tags/0.0.5) - 2024-05-26
### Added
- add Forge (foundry) framework support to projec
- new foundry deployment scripts
- tokenId autoincrement for 1155

## [0.0.4](https://gitlab.com/envelop/mintnft/mintfactory/-/tags/0.0.4) - 2023-12-25
### Added
- Creator in Public Collections can add/remove new minters adresses
- Remove `_ticketContract` param from  deployNewCollection of EventManager


## [0.0.3](https://gitlab.com/envelop/mintnft/mintfactory/-/tags/0.0.3) - 2023-11-26
### Added
- Users SBT factory and implementation
- EventManager contract as case full nft ticket life cycle

## [0.0.2](https://gitlab.com/envelop/mintnft/mintfactory/-/tags/0.0.2) - 2023-09-12
### Fixed
- Fix version of dependencies for off-on feature

## [0.0.1](https://gitlab.com/envelop/mintnft/mintfactory/-/tags/0.0.1) - 2023-09-07
### Added
- Added auto increment tokenId 721
- Burn removed


