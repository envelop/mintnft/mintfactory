// SPDX-License-Identifier: MIT
pragma solidity 0.8.21;
import "@envelop-protocol-v1/contracts/LibEnvelopTypes.sol";
import "../../interfaces/IWrapperV1URI.sol";

contract MockWrapper is IWrapperV1URI {
	string   public baseurl;

	constructor(string memory baseUrl_) 
	    
	{
        baseurl = baseUrl_;
	}

	function getOriginalURI(address _wNFTAddress, uint256 _wNFTTokenId) 
        public 
        view 
        returns(string memory uri_) 
    {
       uri_ = string(
            abi.encodePacked(
                baseurl,
                "/"
            )
        );
    }

    function getWrappedToken(address _wNFTAddress, uint256 _wNFTTokenId) 
        public 
        view 
        returns (ETypes.WNFT memory) 
    {	
    	ETypes.AssetItem[] memory collateral;
    	ETypes.Fee[] memory fee;
    	ETypes.Lock[] memory lock;
    	ETypes.Royalty[] memory royalty;

        return ETypes.WNFT(
        					ETypes.AssetItem(
        									ETypes.Asset(ETypes.AssetType.EMPTY,
        												address(0)),
        									0,
        									0),
        					collateral,
        					address(0),
        					fee,
        					lock,
        					royalty,
        					bytes2(0x0005)
        				);
            
    }
}