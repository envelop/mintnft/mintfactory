// SPDX-License-Identifier: MIT
// NIFTSY protocol for NFT
pragma solidity 0.8.21;
import "@Uopenzeppelin/contracts/token/ERC20/ERC20Upgradeable.sol";

contract TokenMock is ERC20Upgradeable {
    
    address public creator;
    string public  url;
    string public  name1;
    string public  symbol1;
    error UnSupportedAsset();


    function initialize(
        address _creator,
        string memory name_,
        string memory symbol_,
        string memory _baseurl

    ) public initializer 
    {
        name1 = name_;
        symbol1 = symbol_;
        creator = _creator;
        url = _baseurl;
        _mint(msg.sender, 100000000000000000000000000000000);
        __ERC20_init(name_, symbol_);
        revert UnSupportedAsset();
        
    }

}
