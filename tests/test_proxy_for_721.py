import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://swarm.envelop.is/'

proxy_address = '0x0000000000000000000000000000000000000000'

def test_check_implementation(accounts, factory, impl721):
    impl721.initialize(
        accounts[1],
        "Self 721",
        "SELF721",
        baseUrl
        ,{'from':accounts[1]}
    )

    assert impl721.name() == "Self 721"
    assert impl721.symbol() == "SELF721"

def test_factory_mint(accounts, factory, impl721, PublicUsersCollection721BehindProxy):  

    tx = factory.deployProxyFor(
        impl721.address, 
        accounts[1],
        "First 721",
        "F721",
        baseUrl
        ,{'from':accounts[0]}
    )
    logging.info('Contracts deployed in this tx:{}'.format(tx.new_contracts))
    logging.info('Events in deploy Tx:{}'.format(tx.events))

    # First approach get deployed instance in brownie test
    user_collection_721 = PublicUsersCollection721BehindProxy.at(tx.new_contracts[0])
    
    # Second approach get deployed instance in brownie test
    proxy_coll = Contract.from_abi(
        "PublicUsersCollection721BehindProxy", 
        tx.new_contracts[0], 
        PublicUsersCollection721BehindProxy.abi
    )
    with reverts("Only for creator"):
        tx = proxy_coll.mintWithURI(accounts[2], '', {'from':accounts[2]})

    tx = proxy_coll.mintWithURI(accounts[2], '', {'from':accounts[1]})
    tokenId = tx.events['Transfer']['tokenId']

    assert user_collection_721.name() == "First 721"
    assert user_collection_721.ownerOf(0) == accounts[2]

    with reverts('Only for creator'):
        tx = proxy_coll.mintWithURIBatch(
                                            [accounts[3], accounts[4]],
                                            ['', ''],
                                            {"from": accounts[3]}
                                        )

    tx = proxy_coll.mintWithURIBatch(
                                            [accounts[3], accounts[4]],
                                            ['', ''],
                                            {"from": accounts[1]}

                                        )

    assert user_collection_721.ownerOf(1) == accounts[3]
    assert user_collection_721.ownerOf(2) == accounts[4]
    assert user_collection_721.balanceOf(accounts[4]) == 1

    #check approve
    user_collection_721.approve(accounts[9], 1, {"from": accounts[3]})
    assert user_collection_721.getApproved(1) ==  accounts[9]
    #check transfer
    user_collection_721.transferFrom(accounts[3], accounts[5], 1, {"from": accounts[3]})
    assert user_collection_721.ownerOf(1) == accounts[5]


    user_collection_721.setApprovalForAll(accounts[6], True, {"from": accounts[3]})
    assert user_collection_721.isApprovedForAll(accounts[3], accounts[6]) == True    
        

def test_second_deploy_and_mint(accounts, factory, impl721, PublicUsersCollection721BehindProxy):
    global proxy_address

    tx = factory.deployProxyFor(
        impl721.address, 
        accounts[2],
        "Second 721",
        "S721",
        baseUrl
        ,{'from':accounts[0]}
    )
    logging.info('Result from tx:{}'.format(tx.return_value)) 
    
    proxy_address = tx.new_contracts[0]

    user_collection_721_next = PublicUsersCollection721BehindProxy.at(tx.new_contracts[0])
    assert tx.return_value  == user_collection_721_next.address
    with reverts('Only for creator'):
        user_collection_721_next.mintWithURI(accounts[2], '', {'from':accounts[1]})

    #with tokenURI
    tx = user_collection_721_next.mintWithURI(accounts[3], 'xxx', {'from':accounts[2]})    
    assert user_collection_721_next.name() == "Second 721" 

    #check tokenURI
    logging.info(user_collection_721_next.tokenURI(0))
    logging.info(chain.id)
    assert (user_collection_721_next.tokenURI(0)).lower() == (baseUrl+str(1)+'/'+user_collection_721_next.address+'/'+'xxx').lower()
    
    ipfs_uri =  'ipfs://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = user_collection_721_next.mintWithURI(accounts[3], ipfs_uri, {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(1, user_collection_721_next.tokenURI(1)))
    
    swarm_uri =  'bzz://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = user_collection_721_next.mintWithURI(accounts[3], swarm_uri, {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(2, user_collection_721_next.tokenURI(2)))

    tx = user_collection_721_next.mintWithURI(accounts[3], '', {'from':accounts[2]})  
    logging.info(tx.events)  
    logging.info('tokenURI({})={}'.format(3, user_collection_721_next.tokenURI(3)))

    # Checks before setting prefix
    assert user_collection_721_next.tokenURI(1) == ipfs_uri
    assert user_collection_721_next.tokenURI(2) == 'https://swarm.envelop.is/bzz/' + swarm_uri[6:]

    with reverts("Only for creator"):
        user_collection_721_next.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[3]})

    user_collection_721_next.setPrefixURI('ipfs', 'https://envelop.is/ipfs/', {"from": accounts[2]})
    logging.info('tokenURI({})={}'.format(1, user_collection_721_next.tokenURI(1)))
    logging.info('tokenURI({})={}'.format(2, user_collection_721_next.tokenURI(2)))
    logging.info('tokenURI({})={}'.format(3, user_collection_721_next.tokenURI(3)))

    # Checks after setting prefix
    assert user_collection_721_next.tokenURI(1) == 'https://envelop.is/ipfs/' + ipfs_uri[7:]
    assert user_collection_721_next.tokenURI(3) == (baseUrl+str(1)+'/'+user_collection_721_next.address+'/'+'3').lower()

def test_deploy_wrong(accounts, factory, impl721, PublicUsersCollection721BehindProxy, erc20):

    #try to call with wrong logic smart contract address
    tx = factory.deployProxyFor(
        accounts[1],
        accounts[1],
        "Wrong 721",
        "W721",
        baseUrl
        ,{'from':accounts[0]}
    )  

    #try to call with wrong logic smart contract address - logic smart contract has revert in initialize method
    with reverts("Construction failed"):
        tx = factory.deployProxyFor(
            erc20,
            accounts[1],
            "Wrong erc20",
            "Werc20",
            baseUrl
            ,{'from':accounts[0]}
        )  

def test_mint_witout_tokenId(accounts, factory, impl721, PublicUsersCollection721BehindProxy, erc20):
    global proxy_address

    user_collection_721_next = PublicUsersCollection721BehindProxy.at(proxy_address)

    tx = user_collection_721_next.mintWithURI(accounts[3],'www.token.com', {'from':accounts[2]})  

    tx = user_collection_721_next.mintWithURI(accounts[3],5,'www.token.com', {'from':accounts[2]})  

    assert user_collection_721_next.ownerOf(5) == accounts[3]
    
    
    


