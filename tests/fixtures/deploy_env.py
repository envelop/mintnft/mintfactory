import pytest


@pytest.fixture(scope="module")
def impl721(accounts, PublicUsersCollection721BehindProxy):
    nft721 = accounts[0].deploy(PublicUsersCollection721BehindProxy)
    yield nft721   

@pytest.fixture(scope="module")
def impl1155(accounts, PublicUsersCollection1155BehindProxy):
    nft1155 = accounts[0].deploy(PublicUsersCollection1155BehindProxy)
    yield nft1155

@pytest.fixture(scope="module")
def factory(accounts, UsersCollectionFactory):
    f = accounts[0].deploy(UsersCollectionFactory)
    yield f    

@pytest.fixture(scope="module")
def erc20(accounts, TokenMock):
    w = accounts[0].deploy(TokenMock)
    yield w

@pytest.fixture(scope="module")
def dai(accounts, MockDai):
    t = accounts[0].deploy(MockDai, 'Mock DAI Token', 'DAI')
    yield t    

#############################################
##   Subscribtiob scope                   ###
#############################################
@pytest.fixture(scope="module")
def sub_reg(pm, accounts):
    sr = pm('dao-envelop/subscription@0.0.3').SubscriptionRegistry
    yield sr.deploy(accounts[0], {'from':  accounts[0]})

@pytest.fixture(scope="module")
def sub_reg1(pm, accounts):
    sr = pm('dao-envelop/subscription@0.0.3').SubscriptionRegistry
    yield sr.deploy(accounts[0], {'from':  accounts[0]})

@pytest.fixture(scope="module")
def agent(pm, accounts):
    ag = pm('dao-envelop/subscription@0.0.3').EnvelopAgentWithRegistry
    yield ag.deploy({'from':  accounts[0]})
##############################################

@pytest.fixture(scope="module")
def registry(accounts, UserCollectionPRegistry, sub_reg):
    r = accounts[0].deploy(UserCollectionPRegistry, sub_reg.address)
    yield r

###############################################
##              SBT Factory                  ##
###############################################
@pytest.fixture(scope="module")
def implSBT721(accounts, UsersSBTCollection721BehindProxy):
    nft721 = accounts[0].deploy(UsersSBTCollection721BehindProxy)
    yield nft721   

@pytest.fixture(scope="module")
def implSBT1155(accounts, UsersSBTCollection1155BehindProxy):
    nft1155 = accounts[0].deploy(UsersSBTCollection1155BehindProxy)
    yield nft1155

@pytest.fixture(scope="module")
def factorySBT(accounts, UsersSBTCollectionFactory):
    f = accounts[0].deploy(UsersSBTCollectionFactory)
    yield f    

@pytest.fixture(scope="module")
def registrySBT(accounts, UsersSBTCollectionRegistry, sub_reg):
    r = accounts[0].deploy(UsersSBTCollectionRegistry, sub_reg.address)
    yield r

@pytest.fixture(scope="module")
def mockWrapper(accounts, MockWrapper):
    r = accounts[0].deploy(MockWrapper, 'https://bugsbunny.com')
    yield r

@pytest.fixture(scope="module")
def eventManager(accounts, EventManager, sub_reg):
    e = accounts[0].deploy(EventManager, sub_reg)
    yield e

#second type sbt 
@pytest.fixture(scope="module")
def wrapperUsers(pm, accounts, eventManager):
    wr = pm('dao-envelop/envelop-protocol-v1@1.3.1').WrapperUsersV1
    wrapper = accounts[0].deploy(wr, eventManager)
    yield wrapper

#first type sbt 
@pytest.fixture(scope="module")
def wrapperUsers1(pm, accounts, registrySBT):
    wr = pm('dao-envelop/envelop-protocol-v1@1.3.1').WrapperUsersV1
    wrapper = accounts[0].deploy(wr, registrySBT)
    yield wrapper

@pytest.fixture(scope="module")
def tickets(pm, accounts):
    """
    NFT 721 with URI
    """
    e = pm('dao-envelop/envelop-protocol-v1@1.3.1').Token721Mock
    t = accounts[0].deploy(e, "Simple NFT with URI", "XXX")
    t.setURI(0, 'https://maxsiz.github.io/')
    #Token721Mock
    yield t 

