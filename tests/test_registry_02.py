import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://swarm.envelop.is/'
PRICE = 1e18

#add minters in proxy contracts - case for lazy minting

def test_check_init_subscr(accounts, factory, sub_reg, agent, registry, dai):
    sub_reg.setAssetForPaymentState(dai, True, {'from':accounts[0]})
    payOptions = [(dai, PRICE, 0)] #without Agent fee
    #subscription with count type tariff
    subscriptionType = (0,0,2,True, accounts[3])
    tariff1 = (subscriptionType, payOptions)
    registry.registerServiceTariff(tariff1,{'from':accounts[0]})
    registry.authorizeAgentForService(agent.address, [0],{"from": accounts[0]})
    pay_amount = payOptions[0][1]*(sub_reg.PERCENT_DENOMINATOR()+sub_reg.platformFeePercent())/sub_reg.PERCENT_DENOMINATOR()
    dai.transfer(accounts[1], pay_amount, {"from": accounts[0]})
    dai.approve(sub_reg.address, pay_amount, {"from": accounts[1]})
    agent.buySubscription(registry.address, 0, 0, accounts[1], accounts[1], {"from": accounts[1]})
    us = registry.checkUserSubscription(accounts[1])
    assert us ==(True, True)

def test_deploy_01(accounts, factory, impl1155, impl721, registry, PublicUsersCollection1155BehindProxy, PublicUsersCollection721BehindProxy):
    registry.addImplementation((4, impl1155.address),{'from':accounts[0]})
    registry.addImplementation((3, impl721.address),{'from':accounts[0]})
    registry.setFactory(factory.address, {'from':accounts[0]})
    factory.setOperatorStatus(registry.address, True, {'from':accounts[0]})

    tx = registry.deployNewCollection(
        impl1155.address, 
        accounts[1],
        'My Collection 1155',
        'MYSYMB1155',
        baseUrl
       ,{'from':accounts[1]}
    )
    
    erc1155 = PublicUsersCollection1155BehindProxy.at(tx.new_contracts[0])

    tx = erc1155.mintWithURI(accounts[2], 1, 3, '', {'from':accounts[1]})
    with reverts('Only for creator'):
        tx = erc1155.mintWithURI(accounts[2], 1, 3, '', {'from':accounts[2]})
    with reverts("Only for creator"):
        erc1155.setMinterStatus(accounts[2], True, {"from": accounts[2]})
    erc1155.setMinterStatus(accounts[2], True, {"from": accounts[1]})
    tx = erc1155.mintWithURI(accounts[2], 2, 3, '', {'from':accounts[2]})

    assert erc1155.balanceOf(accounts[2], 1) == 3
    assert erc1155.balanceOf(accounts[2], 2) == 3


    tx = registry.deployNewCollection(
        impl721.address, 
        accounts[1],
        'My Collection 721',
        'MYSYMB721',
        baseUrl
       ,{'from':accounts[1]}
    )

    erc721 = PublicUsersCollection721BehindProxy.at(tx.new_contracts[0])
    tx = erc721.mintWithURI(accounts[2], '', {'from':accounts[1]})
    with reverts('Only for creator'):
        tx = erc721.mintWithURI(accounts[2], '', {'from':accounts[2]})

    with reverts("Only for creator"):
        erc721.setMinterStatus(accounts[2], True, {"from": accounts[2]})
    erc721.setMinterStatus(accounts[2], True, {"from": accounts[1]})
    
    tx = erc721.mintWithURI(accounts[2], '', {'from':accounts[2]})

    assert erc721.balanceOf(accounts[2]) == 2



