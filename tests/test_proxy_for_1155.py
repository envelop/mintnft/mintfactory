import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://swarm.envelop.is/'


def test_check_implementation(accounts, factory, impl1155):
    impl1155.initialize(
        accounts[1],
        "Self 1155",
        "SELF1155",
        baseUrl
        ,{'from':accounts[1]}
    )

    assert impl1155.name() == "Self 1155"
    assert impl1155.symbol() == "SELF1155"

def test_factory_mint(accounts, factory, impl1155, PublicUsersCollection1155BehindProxy):  

    tx = factory.deployProxyFor(
        impl1155.address, 
        accounts[1],
        "First 1155",
        "F1155",
        baseUrl
        ,{'from':accounts[0]}
    )
    logging.info('Contracts deployed in this tx:{}'.format(tx.new_contracts))
    logging.info('Events in deploy Tx:{}'.format(tx.events))

    # First approach get deployed instance in brownie test
    user_collection_1155 = PublicUsersCollection1155BehindProxy.at(tx.new_contracts[0])
    
    # Second approach get deployed instance in brownie test
    proxy_coll = Contract.from_abi(
        "PublicUsersCollection1155BehindProxy", 
        tx.new_contracts[0], 
        PublicUsersCollection1155BehindProxy.abi
    )
    with reverts("Only for creator"):
        tx = proxy_coll.mintWithURI(accounts[2], 1, 3, '', {'from':accounts[2]})

    tx = proxy_coll.mintWithURI(accounts[2], 1, 3, '', {'from':accounts[1]})
    tokenId = tx.events['TransferSingle']['id']

    assert user_collection_1155.name() == "First 1155"
    assert user_collection_1155.balanceOf(accounts[2], tokenId) == 3

    with reverts('Only for creator'):
        tx = proxy_coll.mintWithURIBatch(
                                            [accounts[3], accounts[4]],
                                            [2, 3],
                                            [5, 6],
                                            ['', ''],
                                            {"from": accounts[3]}
                                        )

    tx = proxy_coll.mintWithURIBatch(
                                            [accounts[3], accounts[4]],
                                            [2, 3],
                                            [5, 6],
                                            ['', ''],
                                            {"from": accounts[1]}

                                        )

    assert user_collection_1155.balanceOf(accounts[3], 2) == 5
    assert user_collection_1155.balanceOf(accounts[4], 3) == 6
    assert user_collection_1155.balanceOfBatch([accounts[3], accounts[4]], [2,3]) == [5,6]

    #check approve
    user_collection_1155.setApprovalForAll(accounts[9], True, {"from": accounts[3]})
    assert user_collection_1155.isApprovedForAll(accounts[3], accounts[9]) == True
    #check transfer
    user_collection_1155.safeTransferFrom(accounts[3], accounts[5], 2, 1, '', {"from": accounts[3]})
    assert user_collection_1155.balanceOf(accounts[5], 2) == 1
    user_collection_1155.safeBatchTransferFrom(
                                                accounts[3], 
                                                accounts[6], 
                                                [2],
                                                [2],
                                                '',
                                                {"from": accounts[3]}
                                            )

    assert user_collection_1155.balanceOf(accounts[6], 2) == 2
    assert user_collection_1155.balanceOf(accounts[3], 2) == 2

    user_collection_1155.burn(2, 2, {"from": accounts[3]})
    assert user_collection_1155.balanceOf(accounts[3], 2) == 0
        
        

def test_second_deploy_and_mint(accounts, factory, impl1155, PublicUsersCollection1155BehindProxy):
    tx = factory.deployProxyFor(
        impl1155.address, 
        accounts[2],
        "Second 1155",
        "S1155",
        baseUrl
        ,{'from':accounts[0]}
    )
    logging.info('Result from tx:{}'.format(tx.return_value)) 
    user_collection_1155_next = PublicUsersCollection1155BehindProxy.at(tx.new_contracts[0])
    assert tx.return_value  == user_collection_1155_next.address
    with reverts('Only for creator'):
        user_collection_1155_next.mintWithURI(accounts[2], 1, 4, '', {'from':accounts[1]})

    #with tokenURI
    tx = user_collection_1155_next.mintWithURI(accounts[3], 1, 4, 'xxx', {'from':accounts[2]})    
    assert user_collection_1155_next.name() == "Second 1155" 
    assert user_collection_1155_next.balanceOf(accounts[3], 1) == 4
    assert user_collection_1155_next.totalSupply(1) == 4

    assert user_collection_1155_next.exists(1) == True
    #check tokenUri
    logging.info(user_collection_1155_next.uri(1))
    logging.info(chain.id)
    assert (user_collection_1155_next.uri(1)).lower() == (baseUrl+str(1)+'/'+user_collection_1155_next.address+'/'+'xxx').lower()
    
    ipfs_uri =  'ipfs://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = user_collection_1155_next.mintWithURI(accounts[3], 2, 4, ipfs_uri, {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(2, user_collection_1155_next.uri(2)))
    
    swarm_uri =  'bzz://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = user_collection_1155_next.mintWithURI(accounts[3], 3, 4, swarm_uri, {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(3, user_collection_1155_next.uri(3)))

    tx = user_collection_1155_next.mintWithURI(accounts[3], 4, 4, '', {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(4, user_collection_1155_next.uri(4)))

    # Checks before seting prefix
    assert user_collection_1155_next.uri(2) == ipfs_uri
    assert user_collection_1155_next.uri(3) == 'https://swarm.envelop.is/bzz/' + swarm_uri[6:]

    with reverts("Only for creator"):
        user_collection_1155_next.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[3]})

    user_collection_1155_next.setPrefixURI('ipfs', 'https://envelop.is/ipfs/', {"from": accounts[2]})
    logging.info('tokenURI({})={}'.format(2, user_collection_1155_next.uri(2)))
    logging.info('tokenURI({})={}'.format(3, user_collection_1155_next.uri(3)))
    logging.info('tokenURI({})={}'.format(4, user_collection_1155_next.uri(4)))

    # Checks after setting prefix
    assert user_collection_1155_next.uri(2) == 'https://envelop.is/ipfs/' + ipfs_uri[7:]
    assert user_collection_1155_next.uri(4) == (baseUrl+str(1)+'/'+user_collection_1155_next.address+'/'+'4').lower()

def test_deploy_wrong(accounts, factory, impl1155, PublicUsersCollection1155BehindProxy, erc20):

    #try to call with wrong logic smart contract address
    tx = factory.deployProxyFor(
        accounts[1],
        accounts[1],
        "Wrong 1155",
        "W1155",
        baseUrl
        ,{'from':accounts[0]}
    )  

    #try to call with wrong logic smart contract address - logic smart contract has revert in initialize method
    with reverts("Construction failed"):
        tx = factory.deployProxyFor(
            erc20,
            accounts[1],
            "Wrong erc20",
            "Werc20",
            baseUrl
            ,{'from':accounts[0]}
        )  

    
    
    
    


