import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://swarm.envelop.is/'
PRICE = 1e18


def test_check_init_subscr(accounts, factory, sub_reg, agent, registry, dai):
    sub_reg.setAssetForPaymentState(dai, True, {'from':accounts[0]})
    payOptions = [(dai, PRICE, 0)] #without Agent fee
    #subscription with count type tariff
    subscriptionType = (0,0,2,True, accounts[3])
    tariff1 = (subscriptionType, payOptions)
    registry.registerServiceTariff(tariff1,{'from':accounts[0]})
    registry.authorizeAgentForService(agent.address, [0],{"from": accounts[0]})
    pay_amount = payOptions[0][1]*(sub_reg.PERCENT_DENOMINATOR()+sub_reg.platformFeePercent())/sub_reg.PERCENT_DENOMINATOR()
    dai.transfer(accounts[1], pay_amount, {"from": accounts[0]})
    dai.approve(sub_reg.address, pay_amount, {"from": accounts[1]})
    agent.buySubscription(registry.address, 0, 0, accounts[1], accounts[1], {"from": accounts[1]})
    us = registry.checkUserSubscription(accounts[1])
    assert us ==(True, True)

def test_deploy_01(accounts, factory, impl1155, impl721, registry):
    with reverts("This implementation address is not supported"):
        registry.deployNewCollection(
            impl1155.address, 
            accounts[1],
            'My Collection',
            'MYSYMB',
            baseUrl
           ,{'from':accounts[1]}
        )
    r =  registry.isImplementationSupported(impl1155)   
    logging.info('isImplementationSupported({}):{}'.format(impl1155.address, r))    
    with reverts("Ownable: caller is not the owner"):
        registry.addImplementation((4, impl1155.address),{'from':accounts[1]})
    registry.addImplementation((4, impl1155.address),{'from':accounts[0]})
    registry.addImplementation((3, impl721.address),{'from':accounts[0]})
    with reverts("Already exist"):
        registry.addImplementation((4, impl1155.address),{'from':accounts[0]})

    with reverts("Ownable: caller is not the owner"):
        registry.setFactory(factory.address, {'from':accounts[1]})
    registry.setFactory(factory.address, {'from':accounts[0]})
    with reverts("Ownable: caller is not the owner"):
        factory.setOperatorStatus(registry.address, True, {'from':accounts[1]})
    factory.setOperatorStatus(registry.address, True, {'from':accounts[0]})

    tx = registry.deployNewCollection(
        impl1155.address, 
        accounts[1],
        'My Collection 1155',
        'MYSYMB1155',
        baseUrl
       ,{'from':accounts[1]}
    )
    assert len(registry.getUsersCollections(accounts[1])) == 1
    assert registry.getUsersCollections(accounts[1])[0][1] == tx.new_contracts[0]
    assert registry.getUsersCollections(accounts[1])[0][0] == 4
    logging.info(registry.getUsersCollections(accounts[1]))

    assert registry.getSupportedImplementation() == [(4, impl1155.address), (3, impl721.address)]

    tx = registry.deployNewCollection(
        impl721.address, 
        accounts[1],
        'My Collection 721',
        'MYSYMB721',
        baseUrl
       ,{'from':accounts[1]}
    )

    assert len(registry.getUsersCollections(accounts[1])) == 2
    assert registry.getUsersCollections(accounts[1])[1][1] == tx.new_contracts[0]
    assert registry.getUsersCollections(accounts[1])[1][0] == 3

    with reverts("Valid ticket not found"):
        registry.deployNewCollection(
            impl1155.address, 
            accounts[1],
            'My Collection',
            'MYSYMB',
            baseUrl
           ,{'from':accounts[1]}
        )

    with reverts("Ownable: caller is not the owner"): 
        registry.removeImplementationByIndex(0, {"from": accounts[1]})

    registry.removeImplementationByIndex(0, {"from": accounts[0]})
    assert registry.getSupportedImplementation() == [(3, impl721.address)]
    assert registry.isImplementationSupported(impl1155) == (False, 0)




