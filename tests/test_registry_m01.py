import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://swarm.envelop.is/'
PRICE = 1e18


def test_check_init_subscr(accounts, factory, impl721, sub_reg, agent, registry, dai):
    sub_reg.setAssetForPaymentState(dai, True, {'from':accounts[0]})
    payOptions = [(dai, PRICE, 0)] #without Agent fee
    subscriptionType = (0,1000,0,True, accounts[3])
    tariff1 = (subscriptionType, payOptions)
    registry.registerServiceTariff(tariff1,{'from':accounts[0]})
    registry.authorizeAgentForService(agent.address, [0],{"from": accounts[0]})
    pay_amount = payOptions[0][1]*(sub_reg.PERCENT_DENOMINATOR()+sub_reg.platformFeePercent())/sub_reg.PERCENT_DENOMINATOR()
    dai.transfer(accounts[1], pay_amount, {"from": accounts[0]})
    dai.approve(sub_reg.address, pay_amount, {"from": accounts[1]})
    agent.buySubscription(registry.address, 0, 0, accounts[1], accounts[1], {"from": accounts[1]})
    us = registry.checkUserSubscription(accounts[1])
    assert us ==(True, False)

def test_deploy_01(accounts, factory, impl721, registry):
    with reverts("This implementation address is not supported"):
        registry.deployNewCollection(
            impl721.address, 
            accounts[1],
            'My Collection',
            'MYSYMB',
            baseUrl
           ,{'from':accounts[1]}
        )
    r =  registry.isImplementationSupported(impl721)   
    logging.info('isImplementationSupported({}):{}'.format(impl721.address, r))    
    registry.addImplementation((3, impl721.address),{'from':accounts[0]})
    with reverts("Already exist"):
        registry.addImplementation((3, impl721.address),{'from':accounts[0]})

    registry.setFactory(factory.address, {'from':accounts[0]})
    factory.setOperatorStatus(registry.address, True, {'from':accounts[0]})
    registry.deployNewCollection(
        impl721.address, 
        accounts[1],
        'My Collection',
        'MYSYMB',
        baseUrl
       ,{'from':accounts[1]}
    )
    assert len(registry.getUsersCollections(accounts[1])) == 1
    logging.info(registry.getUsersCollections(accounts[1]))

    chain.sleep(1000)
    chain.mine()

    with reverts("Valid ticket not found"):
        registry.deployNewCollection(
            impl721.address, 
            accounts[1],
            'My Collection',
            'MYSYMB',
            baseUrl
           ,{'from':accounts[1]}
        )



