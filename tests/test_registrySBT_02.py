import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
#baseUrl = 'https://swarm.envelop.is/'
baseUrl = 'ipfs://bafkreiagbvkl4iwibzd4fsyiyynoxsjjbrpsxd7uniy5hzyyre7yldvtnq'
PRICE = 1e18


#check gift case
#check wrapping empty and tokenUri
def test_check_init_subscr(accounts, factorySBT, sub_reg, agent, registrySBT, dai):
    sub_reg.setAssetForPaymentState(dai, True, {'from':accounts[0]})
    payOptions = [(dai, PRICE, 0)] #without Agent fee
    #subscription with count type tariff
    subscriptionType = (0,0,2,True, accounts[3])
    tariff1 = (subscriptionType, payOptions)
    registrySBT.registerServiceTariff(tariff1,{'from':accounts[0]})
    registrySBT.authorizeAgentForService(agent.address, [0],{"from": accounts[0]})
    pay_amount = payOptions[0][1]*(sub_reg.PERCENT_DENOMINATOR()+sub_reg.platformFeePercent())/sub_reg.PERCENT_DENOMINATOR()
    dai.transfer(accounts[1], pay_amount, {"from": accounts[0]})
    dai.approve(sub_reg.address, pay_amount, {"from": accounts[1]})
    agent.buySubscription(registrySBT.address, 0, 0, accounts[1], accounts[1], {"from": accounts[1]})
    us = registrySBT.checkUserSubscription(accounts[1])
    assert us ==(True, True)

def test_deploy_01(accounts, factorySBT, implSBT1155, implSBT721, registrySBT, sub_reg, wrapperUsers1, tickets, UsersSBTCollection721BehindProxy, dai):
    
    registrySBT.addImplementation((4, implSBT1155.address),{'from':accounts[0]})
    registrySBT.addImplementation((3, implSBT721.address),{'from':accounts[0]})
    registrySBT.setFactory(factorySBT.address, {'from':accounts[0]})
    factorySBT.setOperatorStatus(registrySBT.address, True, {'from':accounts[0]})

    tx = registrySBT.deployNewCollection(
        implSBT721.address, 
        accounts[1],
        'My Collection 721',
        'MYSYMB721',
        baseUrl,
        wrapperUsers1, #_wrapper
       {'from':accounts[1]}
    )

    sbtContract = tx.new_contracts[0]

    assert len(registrySBT.getUsersCollections(accounts[1])) == 1
    assert registrySBT.getUsersCollections(accounts[1])[0][1] == tx.new_contracts[0]
    assert registrySBT.getUsersCollections(accounts[1])[0][0] == 3

    dai.transfer(accounts[1], 100)
    dai.approve(wrapperUsers1, 100, {"from": accounts[1]})

    empty_property = (1, zero_address)
    empty_data = (empty_property, 0, 0)
    fee = []
    lock = []
    royalty = []

    wNFT = ( empty_data,
        accounts[2],
        fee,
        lock,
        royalty,
        3,
        0,
        Web3.toBytes(0x0000)  #rules - NO rules
        )
    #with collateral
    tx = wrapperUsers1.wrapIn(wNFT, [((2,dai),0,100)], accounts[2], sbtContract,  {"from": accounts[1]})

    wTokenId = tx.return_value[1]
    wnft721 = UsersSBTCollection721BehindProxy.at(sbtContract)

    assert wrapperUsers1.getWrappedToken(sbtContract, wTokenId)[6] == '0x0000' # rules was not updated
    assert dai.balanceOf(wrapperUsers1) == 100
    logging.info(wnft721.tokenURI(wTokenId))
    assert wrapperUsers1.getCollateralBalanceAndIndex(
            sbtContract, 
            wTokenId,
            2,
            dai,
            0)[0] == 100

    #try to add collateral by not owner of sbt conllection
    dai.transfer(accounts[2], 100)
    dai.approve(wrapperUsers1, 100, {"from": accounts[2]})

    with reverts('Only wNFT contract owner able to add collateral'):
        wrapperUsers1.addCollateral(sbtContract, wTokenId, [((2,dai),0,100)], {"from":accounts[2]})


    

 










