import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://swarm.envelop.is/'
PRICE = 1e18


def test_check_init_subscr(accounts, factorySBT, sub_reg, agent, registrySBT, dai):
    sub_reg.setAssetForPaymentState(dai, True, {'from':accounts[0]})
    payOptions = [(dai, PRICE, 0)] #without Agent fee
    #subscription with count type tariff
    subscriptionType = (0,0,2,True, accounts[3])
    tariff1 = (subscriptionType, payOptions)
    registrySBT.registerServiceTariff(tariff1,{'from':accounts[0]})
    registrySBT.authorizeAgentForService(agent.address, [0],{"from": accounts[0]})
    pay_amount = payOptions[0][1]*(sub_reg.PERCENT_DENOMINATOR()+sub_reg.platformFeePercent())/sub_reg.PERCENT_DENOMINATOR()
    dai.transfer(accounts[1], pay_amount, {"from": accounts[0]})
    dai.approve(sub_reg.address, pay_amount, {"from": accounts[1]})
    agent.buySubscription(registrySBT.address, 0, 0, accounts[1], accounts[1], {"from": accounts[1]})
    us = registrySBT.checkUserSubscription(accounts[1])
    assert us ==(True, True)

def test_deploy_01(accounts, factorySBT, implSBT1155, implSBT721, registrySBT, sub_reg):
    with reverts("This implementation address is not supported"):
        registrySBT.deployNewCollection(
            implSBT1155.address, 
            accounts[1],
            'My Collection',
            'MYSYMB',
            baseUrl,
            accounts[1]
           ,{'from':accounts[1]}
        )
    r =  registrySBT.isImplementationSupported(implSBT1155)   
    logging.info('isImplementationSupported({}):{}'.format(implSBT1155.address, r))    
    with reverts("Ownable: caller is not the owner"):
        registrySBT.addImplementation((4, implSBT1155.address),{'from':accounts[1]})
    registrySBT.addImplementation((4, implSBT1155.address),{'from':accounts[0]})
    registrySBT.addImplementation((3, implSBT721.address),{'from':accounts[0]})
    with reverts("Already exist"):
        registrySBT.addImplementation((4, implSBT1155.address),{'from':accounts[0]})

    with reverts("Ownable: caller is not the owner"):
        registrySBT.setFactory(factorySBT.address, {'from':accounts[1]})
    registrySBT.setFactory(factorySBT.address, {'from':accounts[0]})
    with reverts("Ownable: caller is not the owner"):
        factorySBT.setOperatorStatus(registrySBT.address, True, {'from':accounts[1]})
    factorySBT.setOperatorStatus(registrySBT.address, True, {'from':accounts[0]})

    tx = registrySBT.deployNewCollection(
        implSBT1155.address, 
        accounts[1],
        'My Collection 1155',
        'MYSYMB1155',
        baseUrl,
        accounts[1]
       ,{'from':accounts[1]}
    )
    assert len(registrySBT.getUsersCollections(accounts[1])) == 1
    assert registrySBT.getUsersCollections(accounts[1])[0][1] == tx.new_contracts[0]
    assert registrySBT.getUsersCollections(accounts[1])[0][0] == 4
    logging.info(registrySBT.getUsersCollections(accounts[1]))

    assert registrySBT.getSupportedImplementation() == [(4, implSBT1155.address), (3, implSBT721.address)]

    tx = registrySBT.deployNewCollection(
        implSBT721.address, 
        accounts[1],
        'My Collection 721',
        'MYSYMB721',
        baseUrl,
        accounts[1]
       ,{'from':accounts[1]}
    )

    assert len(registrySBT.getUsersCollections(accounts[1])) == 2
    assert registrySBT.getUsersCollections(accounts[1])[1][1] == tx.new_contracts[0]
    assert registrySBT.getUsersCollections(accounts[1])[1][0] == 3

    with reverts("Valid ticket not found"):
        registrySBT.deployNewCollection(
            implSBT1155.address, 
            accounts[1],
            'My Collection',
            'MYSYMB',
            baseUrl,
            accounts[1]
           ,{'from':accounts[1]}
        )

    with reverts("Ownable: caller is not the owner"): 
        registrySBT.removeImplementationByIndex(0, {"from": accounts[1]})

    registrySBT.removeImplementationByIndex(0, {"from": accounts[0]})
    assert registrySBT.getSupportedImplementation() == [(3, implSBT721.address)]
    assert registrySBT.isImplementationSupported(implSBT1155) == (False, 0)

    #swith off subscription
    with reverts("Ownable: caller is not the owner"):
        registrySBT.setSubscriptionOnOff(False, {"from": accounts[1]})
    registrySBT.setSubscriptionOnOff(False, {"from": accounts[0]})

    tx = registrySBT.deployNewCollection(
        implSBT721.address, 
        accounts[1],
        'My Collection 721',
        'MYSYMB721_1',
        baseUrl,
        accounts[1]
       ,{'from':accounts[1]}
    )

    chain.sleep(100)
    logging.info(registrySBT.checkUserSubscription(accounts[2]))
    logging.info(sub_reg.getUserTicketForService(registrySBT, accounts[2]))
    logging.info(sub_reg.checkUserSubscription(accounts[2], registrySBT))



