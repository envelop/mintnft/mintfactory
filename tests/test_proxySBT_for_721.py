import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://swarm.envelop.is/'

proxy_address = '0x0000000000000000000000000000000000000000'

def test_check_implementation(accounts, factorySBT, implSBT721):
    implSBT721.initialize(
        accounts[1],
        "Self 721",
        "SELF721",
        baseUrl,
        accounts[1]  #wrapper
        ,{'from':accounts[1]}
    )

    assert implSBT721.name() == "Self 721"
    assert implSBT721.symbol() == "SELF721"
    assert implSBT721.owner() == accounts[1]

def test_factorySBT_mint(accounts, factorySBT, implSBT721, UsersSBTCollection721BehindProxy):  

    tx = factorySBT.deployProxyFor(
        implSBT721.address, 
        accounts[1],
        "First 721",
        "wF721",
        baseUrl,
        accounts[1] #wrapper
        ,{'from':accounts[0]}
    )
    logging.info('Contracts deployed in this tx:{}'.format(tx.new_contracts))
    logging.info('Events in deploy Tx:{}'.format(tx.events))

    # First approach get deployed instance in brownie test
    user_collection_721 = UsersSBTCollection721BehindProxy.at(tx.new_contracts[0])
    
    # Second approach get deployed instance in brownie test
    proxy_coll = Contract.from_abi(
        "UsersSBTCollection721BehindProxy", 
        tx.new_contracts[0], 
        UsersSBTCollection721BehindProxy.abi
    )
    with reverts("Trusted address only"):
        tx = proxy_coll.mintWithRules(accounts[2], Web3.toBytes(0x0005), {'from':accounts[2]})

    # It possible since 0.0.3
    # with reverts("SBT MUST Have rule"):
    #     tx = proxy_coll.mintWithRules(accounts[2], '0', {'from':accounts[1]})

    tx = proxy_coll.mintWithRules(accounts[2], Web3.toBytes(0x0005), {'from':accounts[1]})
    tokenId = tx.events['Transfer']['tokenId']
    assert user_collection_721.totalSupply() == 1

    with reverts("Trusted address only"):
        user_collection_721.burn(tokenId, {"from": accounts[2]})

    with reverts("UnWrap was disabled by author"):
        user_collection_721.burn(tokenId, {"from": accounts[1]})

    with reverts("Transfer was disabled by author"):
        user_collection_721.transferFrom(accounts[2], accounts[5], tokenId, {"from": accounts[2]})

    assert user_collection_721.name() == "First 721"
    assert user_collection_721.ownerOf(tokenId) == accounts[2]

    #check approve
    tx = proxy_coll.mintWithRules(accounts[2], Web3.toBytes(0x0001), {'from':accounts[1]})
    tokenId = tx.events['Transfer']['tokenId']
    assert user_collection_721.totalSupply() == 2
    assert user_collection_721.tokenByIndex(0) == tokenId-1


    assert user_collection_721.exists(tokenId) == True
    user_collection_721.approve(accounts[9], tokenId, {"from": accounts[2]})
    assert user_collection_721.getApproved(tokenId) ==  accounts[9]
    #check transfer
    user_collection_721.transferFrom(accounts[2], accounts[5], tokenId, {"from": accounts[2]})
    assert user_collection_721.ownerOf(tokenId) == accounts[5]


    user_collection_721.setApprovalForAll(accounts[6], True, {"from": accounts[5]})
    assert user_collection_721.isApprovedForAll(accounts[5], accounts[6]) == True 
    
    tx = proxy_coll.mintWithRules(accounts[2], Web3.toBytes(0x0004), {'from':accounts[1]})
    tokenId = tx.events['Transfer']['tokenId']  
    user_collection_721.burn(tokenId, {"from": accounts[1]})
    with reverts("ERC721: invalid token ID"):
        user_collection_721.ownerOf(tokenId) 

        

def test_second_deploy_and_mint(accounts, factorySBT, implSBT721, UsersSBTCollection721BehindProxy):
    global proxy_address

    tx = factorySBT.deployProxyFor(
        implSBT721.address, 
        accounts[2],
        "Second 721",
        "S721",
        baseUrl,
        accounts[1]  #wrapper
        ,{'from':accounts[0]}
    )
    logging.info('Result from tx:{}'.format(tx.return_value)) 
    
    proxy_address = tx.new_contracts[0]

    user_collection_721_next = UsersSBTCollection721BehindProxy.at(tx.new_contracts[0])
    assert tx.return_value  == user_collection_721_next.address
    with reverts('Trusted address only'):
        user_collection_721_next.mintWithRules(accounts[2], Web3.toBytes(0x0005), {'from':accounts[0]})

    #with tokenURI
    tx = user_collection_721_next.mintWithRules(accounts[3], Web3.toBytes(0x0005), {'from':accounts[1]})    
    assert user_collection_721_next.name() == "Second 721" 

    #check tokenURI
    #logging.info(user_collection_721_next.tokenURI(0))
    #logging.info(chain.id)
    #assert (user_collection_721_next.tokenURI(0)).lower() == (baseUrl+str(1)+'/'+user_collection_721_next.address+'/'+'xxx').lower()
    
    

def test_deploy_wrong(accounts, factorySBT, implSBT721, UsersSBTCollection721BehindProxy, erc20):

    #try to call with wrong logic smart contract address
    tx = factorySBT.deployProxyFor(
        accounts[1],
        accounts[1],
        "Wrong 721",
        "W721",
        baseUrl,
        accounts[1]  #wrapper
        ,{'from':accounts[0]}
    )  

    #try to call with wrong logic smart contract address - logic smart contract has revert in initialize method
    with reverts("Construction failed"):
        tx = factorySBT.deployProxyFor(
            erc20,
            accounts[1],
            "Wrong erc20",
            "Werc20",
            baseUrl,
            accounts[1]  #wrapper
            ,{'from':accounts[0]}
        )  

def test_factorySBT_mint_mock(accounts, factorySBT, implSBT721, UsersSBTCollection721BehindProxy, mockWrapper):  

    tx = factorySBT.deployProxyFor(
        implSBT721.address, 
        accounts[1],
        "First 721",
        "wF721",
        baseUrl,
        mockWrapper #wrapper
        ,{'from':accounts[0]}
    )
    proxy_address = tx.new_contracts[0]

    user_collection_721_1 = UsersSBTCollection721BehindProxy.at(tx.new_contracts[0])

    assert user_collection_721_1.wnftInfo(0)[6] == '0x0005'
    
    
    


