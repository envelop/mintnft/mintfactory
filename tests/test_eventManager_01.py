import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://swarm.envelop.is/'
PRICE = 1e18


def test_check_init_subscr(accounts, factorySBT, sub_reg, agent, eventManager, dai):
    sub_reg.setAssetForPaymentState(dai, True, {'from':accounts[0]})
    payOptions = [(dai, PRICE, 0)] #without Agent fee
    #subscription with count type tariff
    subscriptionType = (0,0,2,True, accounts[3])
    tariff1 = (subscriptionType, payOptions)
    eventManager.registerServiceTariff(tariff1,{'from':accounts[0]})
    eventManager.authorizeAgentForService(agent.address, [0],{"from": accounts[0]})
    pay_amount = payOptions[0][1]*(sub_reg.PERCENT_DENOMINATOR()+sub_reg.platformFeePercent())/sub_reg.PERCENT_DENOMINATOR()
    dai.transfer(accounts[1], pay_amount, {"from": accounts[0]})
    dai.approve(sub_reg.address, pay_amount, {"from": accounts[1]})
    agent.buySubscription(eventManager.address, 0, 0, accounts[1], accounts[1], {"from": accounts[1]})
    us = eventManager.checkUserSubscription(accounts[1])
    assert us ==(True, True)

def test_deploy_01(accounts, factorySBT, implSBT1155, implSBT721, eventManager, sub_reg, wrapperUsers, tickets, UsersSBTCollection721BehindProxy):
    dates1 = ((chain.time()+10, chain.time() + 200),(chain.time() + 300, chain.time() + 400), Web3.toBytes(0x0005), tickets)
    with reverts("This implementation address is not supported"):
        eventManager.deployNewCollection(
            implSBT721.address, 
            accounts[1],
            'My Collection',
            'MYSYMB',
            baseUrl,
            wrapperUsers, #_wrapper
            #tickets, #_ticketContract
            dates1 #_eventDates
           ,{'from':accounts[1]}
        )
    r =  eventManager.isImplementationSupported(implSBT1155)   
    logging.info('isImplementationSupported({}):{}'.format(implSBT1155.address, r))    
    with reverts("Ownable: caller is not the owner"):
        eventManager.addImplementation((4, implSBT1155.address),{'from':accounts[1]})
    eventManager.addImplementation((4, implSBT1155.address),{'from':accounts[0]})
    eventManager.addImplementation((3, implSBT721.address),{'from':accounts[0]})
    with reverts("Already exist"):
        eventManager.addImplementation((4, implSBT1155.address),{'from':accounts[0]})

    with reverts("Ownable: caller is not the owner"):
        eventManager.setFactory(factorySBT.address, {'from':accounts[1]})
    eventManager.setFactory(factorySBT.address, {'from':accounts[0]})
    with reverts("Ownable: caller is not the owner"):
        factorySBT.setOperatorStatus(eventManager.address, True, {'from':accounts[1]})
    factorySBT.setOperatorStatus(eventManager.address, True, {'from':accounts[0]})

    dates1 = (
        (chain.time() + 10, chain.time() + 200),
        (chain.time() + 300, chain.time() + 400),
        Web3.toBytes(0x0004), # SBT Rule,
        tickets
    )

    tx = eventManager.deployNewCollection(
        implSBT721.address, 
        accounts[1],
        'My Collection 721',
        'MYSYMB721',
        baseUrl,
        wrapperUsers, #_wrapper
        #tickets, #_ticketContract
        dates1 #_eventDates
       ,{'from':accounts[1]}
    )

    eventContract = tx.new_contracts[0]

    assert len(eventManager.getUsersCollections(accounts[1])) == 1
    assert eventManager.getUsersCollections(accounts[1])[0][1] == tx.new_contracts[0]
    assert eventManager.getUsersCollections(accounts[1])[0][0] == 3
    assert eventManager.getEventsForTicket(tickets)[0][0] == tx.new_contracts[0]
    assert eventManager.getEventsForTicket(tickets)[1][0][0] == dates1[0]    
    assert eventManager.getEventsForTicket(tickets)[1][0][1] == dates1[1]
    assert eventManager.getEventsForTicket(tickets)[1][0][2] == '0x0004'
    assert eventManager.getDataForEvent(eventContract)[0] == dates1[0]    
    assert eventManager.getDataForEvent(eventContract)[1] == dates1[1]    
    assert eventManager.getDataForEvent(eventContract)[2] == '0x0004'
    assert eventManager.getDataForEvent(eventContract)[3] == tickets

    #wrap time has not started yet
    assert eventManager.isWrapEnabled(tickets, eventContract) == False
    #sbt time has not started yet
    assert eventManager.isRulesUpdateEnabled(eventContract) == '0x0000'

    #nonexist ticket contract
    assert eventManager.isWrapEnabled(accounts[9], eventContract) == False
    
    #move time
    chain.sleep(50)
    chain.mine()

    #wrap time has started
    assert eventManager.isWrapEnabled(tickets, eventContract) == True
    
    #check 
    #prepare data to wrap

    tickets.mint(1, {"from": accounts[2]})

    tickets.approve(wrapperUsers.address, 1, {'from':accounts[2]})

    erc721_property = (3, tickets.address)
    erc721_data = (erc721_property, 1, 0)
    fee = []
    lock = []
    royalty = []

    wNFT = ( erc721_data,
        accounts[2],
        fee,
        lock,
        royalty,
        3,
        0,
        Web3.toBytes(0x0001)  #rules - NO rules
        )

    with reverts('Wrap check fail'):
        tx = wrapperUsers.wrapIn(wNFT, [], accounts[2], eventContract,  {"from": accounts[2], "value": 1})

    wNFT = ( erc721_data,
        accounts[2],
        fee,
        lock,
        royalty,
        3,
        0,
        Web3.toBytes(0x0000)  #rules - NO rules
        )
    
    with reverts('Only wNFT contract owner able to add collateral'):
        tx = wrapperUsers.wrapIn(wNFT, [], accounts[2], eventContract,  {"from": accounts[2], "value": 1})

    tx = wrapperUsers.wrapIn(wNFT, [], accounts[2], eventContract,  {"from": accounts[2]})

    wTokenId = tx.return_value[1]


    #not owner tries to update rules
    with reverts("Only wNFT owner can upgrade rules"):
        wrapperUsers.upgradeRules(((3,eventContract),wTokenId,0), {"from": accounts[0]})

    #try to update to sbt - time has not started yet
    wrapperUsers.upgradeRules(((3,eventContract),wTokenId,0), {"from": accounts[2]})
    assert wrapperUsers.getWrappedToken(eventContract, wTokenId)[6] == '0x0000' # rules was not updated


    chain.sleep(300)
    chain.mine()

    #sbt time has started - let's got to update rules
    assert eventManager.isRulesUpdateEnabled(eventContract) == '0x0004'

    wrapperUsers.upgradeRules(((3,eventContract),wTokenId,0), {"from": accounts[2]})

    assert wrapperUsers.getWrappedToken(eventContract, wTokenId)[6] == '0x0004'

    assert eventManager.getSupportedImplementation() == [(4, implSBT1155.address), (3, implSBT721.address)]

    #try to update rules secont time
    with reverts('Only once to SBT'):
        wrapperUsers.upgradeRules(((3,eventContract),wTokenId,0), {"from": accounts[2]})

    dates2 = ((chain.time() + 20, chain.time() + 30),(chain.time() + 40, chain.time() + 50), Web3.toBytes(0x0001), tickets)
    tx = eventManager.deployNewCollection(
        implSBT721.address, 
        accounts[1],
        'My Collection 721',
        'MYSYMB721',
        baseUrl,
        wrapperUsers, #_wrapper
        #tickets, #_ticketContract
        dates2 #_eventDates
       ,{'from':accounts[1]}
    )

    eventContract = tx.new_contracts[0]

    assert len(eventManager.getUsersCollections(accounts[1])) == 2
    assert eventManager.getUsersCollections(accounts[1])[1][1] == eventContract
    assert eventManager.getUsersCollections(accounts[1])[1][0] == 3
    assert eventManager.getEventsForTicket(tickets)[0][1] == eventContract
    assert eventManager.getEventsForTicket(tickets)[1][1][0] == dates2[0]
    assert eventManager.getEventsForTicket(tickets)[1][1][1] == dates2[1]
    assert eventManager.getEventsForTicket(tickets)[1][1][2] == '0x0001'
    assert eventManager.getDataForEvent(eventContract)[0] == dates2[0]
    assert eventManager.getDataForEvent(eventContract)[1] == dates2[1]
    assert eventManager.getDataForEvent(eventContract)[2] == '0x0001'
    assert eventManager.getDataForEvent(eventContract)[3] == tickets

    event1 = UsersSBTCollection721BehindProxy.at(eventContract)

    with reverts("Valid ticket not found"):
        eventManager.deployNewCollection(
            implSBT1155.address, 
            accounts[1],
            'My Collection',
            'MYSYMB',
            baseUrl,
            accounts[1], #_wrapper
            #accounts[3], #_ticketContract
            dates2 #_eventDates
           ,{'from':accounts[1]}
        )

    with reverts("Ownable: caller is not the owner"): 
        eventManager.removeImplementationByIndex(0, {"from": accounts[1]})

    eventManager.removeImplementationByIndex(0, {"from": accounts[0]})
    assert eventManager.getSupportedImplementation() == [(3, implSBT721.address)]
    assert eventManager.isImplementationSupported(implSBT1155) == (False, 0)

    #swith off subscription
    with reverts("Ownable: caller is not the owner"):
        eventManager.setSubscriptionOnOff(False, {"from": accounts[1]})
    eventManager.setSubscriptionOnOff(False, {"from": accounts[0]})

    dates3 = ((chain.time() + 30, chain.time() + 40),(chain.time() + 50, chain.time() + 60), Web3.toBytes(0x0004), tickets)
    tx = eventManager.deployNewCollection(
        implSBT721.address, 
        accounts[1],
        'My Collection 721',
        'MYSYMB721_1',
        baseUrl,
        wrapperUsers, #_wrapper
        #tickets, #_ticketContract
        dates3 #_eventDates
       ,{'from':accounts[1]}
    )

    eventContract = tx.new_contracts[0]
    event2 = UsersSBTCollection721BehindProxy.at(eventContract)

    #there are three event contracts for one ticket contract
    assert eventManager.getEventsForTicket(tickets)[0][1] == event1.address
    assert eventManager.getEventsForTicket(tickets)[0][2] == event2.address
    assert eventManager.getEventsForTicket(tickets)[1][1][0] == dates2[0]
    assert eventManager.getEventsForTicket(tickets)[1][1][1] == dates2[1]
    assert eventManager.getEventsForTicket(tickets)[1][1][2] == '0x0001'
    assert eventManager.getEventsForTicket(tickets)[1][2][0] == dates3[0]
    assert eventManager.getEventsForTicket(tickets)[1][2][1] == dates3[1]
    assert eventManager.getEventsForTicket(tickets)[1][2][2] == '0x0004'
    assert eventManager.getDataForEvent(event2.address)[0] == dates3[0]
    assert eventManager.getDataForEvent(event2.address)[1] == dates3[1]
    assert eventManager.getDataForEvent(event2.address)[2] == '0x0004'
    assert eventManager.getDataForEvent(event2.address)[3] == tickets

    dates4 = ((chain.time() + 100, chain.time() + 200),(chain.time() + 300, chain.time() + 400), Web3.toBytes(0x0001), tickets)
    with reverts(''):
        eventManager.editDatesForEvent(event1.address, dates4, {"from": accounts[9]})

    eventManager.editDatesForEvent(event1.address, dates4, {"from": accounts[1]})
    assert eventManager.getDataForEvent(event1.address)[0] == dates4[0]
    assert eventManager.getDataForEvent(event1.address)[1] == dates4[1]
    assert eventManager.getDataForEvent(event1.address)[2] == '0x0001'
    assert eventManager.getDataForEvent(event1.address)[3] == tickets


    #check boundary conditions of time period

    #create second ticket
    tickets.mint(2, {"from": accounts[3]})
    tickets.approve(wrapperUsers.address, 2, {'from':accounts[3]})

    erc721_property = (3, tickets.address)
    erc721_data = (erc721_property, 2, 0)
    fee = []
    lock = []
    royalty = []

    wNFT = ( erc721_data,
        accounts[3],
        fee,
        lock,
        royalty,
        3,
        0,
        Web3.toBytes(0x0000)  #rules - NO rules
        )

    #before wrap time
    with reverts('Wrap check fail'):
        tx = wrapperUsers.wrapIn(wNFT, [], accounts[3], eventContract,  {"from": accounts[3]})

    chain.sleep(35)
    chain.mine()

    tx = wrapperUsers.wrapIn(wNFT, [], accounts[3], eventContract,  {"from": accounts[3]})
    wTokenId = tx.return_value[1]

    chain.sleep(500)
    chain.mine()

    #miss time to update to sbt
    wrapperUsers.upgradeRules(((3,eventContract),wTokenId,0), {"from": accounts[3]})
    assert wrapperUsers.getWrappedToken(eventContract, wTokenId)[6] == '0x0000'

    #create third ticket
    tickets.mint(3, {"from": accounts[3]})
    tickets.approve(wrapperUsers.address, 3, {'from':accounts[3]})

    erc721_property = (3, tickets.address)
    erc721_data = (erc721_property, 3, 0)
    fee = []
    lock = []
    royalty = []

    wNFT = ( erc721_data,
        accounts[3],
        fee,
        lock,
        royalty,
        3,
        0,
        Web3.toBytes(0x0000)  #rules - NO rules
        )

    #miss time to wrap
    with reverts("Wrap check fail"):
        tx = wrapperUsers.wrapIn(wNFT, [], accounts[3], eventContract,  {"from": accounts[3]})










