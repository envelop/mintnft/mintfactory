import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://swarm.envelop.is/'


def test_check_implementation(accounts, factorySBT, implSBT1155):
    implSBT1155.initialize(
        accounts[1],
        "Self 1155",
        "SELF1155",
        baseUrl,
        accounts[1]  #wrapper
        ,{'from':accounts[1]}
    )

    assert implSBT1155.name() == "Self 1155"
    assert implSBT1155.symbol() == "SELF1155"

def test_factorySBT_mint(accounts, factorySBT, implSBT1155, UsersSBTCollection1155BehindProxy):  

    tx = factorySBT.deployProxyFor(
        implSBT1155.address, 
        accounts[1],
        "First 1155",
        "F1155",
        baseUrl, 
        accounts[1]  #wrapper
        ,{'from':accounts[0]}
    )
    logging.info('Contracts deployed in this tx:{}'.format(tx.new_contracts))
    logging.info('Events in deploy Tx:{}'.format(tx.events))

    # First approach get deployed instance in brownie test
    user_collection_1155 = UsersSBTCollection1155BehindProxy.at(tx.new_contracts[0])
    
    # Second approach get deployed instance in brownie test
    proxy_coll = Contract.from_abi(
        "UsersSBTCollection1155BehindProxy", 
        tx.new_contracts[0], 
        UsersSBTCollection1155BehindProxy.abi
    )
    with reverts("Trusted address only"):
        tx = proxy_coll.mintWithRules(accounts[2], 3, Web3.toBytes(0x0005), {'from':accounts[2]})
    tx = proxy_coll.mintWithRules(accounts[2], 3,  Web3.toBytes(0x0005), {'from':accounts[1]})
    tokenId = tx.events['TransferSingle']['id']

    assert user_collection_1155.name() == "First 1155"
    assert user_collection_1155.balanceOf(accounts[2], tokenId) == 3

    with reverts("Transfer was disabled by author"):
        user_collection_1155.safeTransferFrom(accounts[2], accounts[5], tokenId, 1, '', {"from": accounts[2]})

    tx = proxy_coll.mintWithRules(accounts[2], 3,  Web3.toBytes(0x0001), {'from':accounts[1]})
    tokenId = tx.events['TransferSingle']['id']

    #check approve
    user_collection_1155.setApprovalForAll(accounts[9], True, {"from": accounts[2]})
    assert user_collection_1155.isApprovedForAll(accounts[2], accounts[9]) == True
    #check transfer
    user_collection_1155.safeTransferFrom(accounts[2], accounts[5], tokenId, 1, '', {"from": accounts[9]})
    assert user_collection_1155.balanceOf(accounts[5], tokenId) == 1
    user_collection_1155.safeBatchTransferFrom(
                                                accounts[2], 
                                                accounts[6], 
                                                [tokenId],
                                                [1],
                                                '',
                                                {"from": accounts[2]}
                                            )

    assert user_collection_1155.balanceOf(accounts[6], tokenId) == 1
    assert user_collection_1155.balanceOf(accounts[2], tokenId) == 1

    with reverts("Trusted address only"):
        user_collection_1155.burn(accounts[2], tokenId, 1, {"from": accounts[2]})
    with reverts("UnWrap was disabled by author"):
        user_collection_1155.burn(accounts[2], tokenId, 1, {"from": accounts[1]})
    

    tx = proxy_coll.mintWithRules(accounts[2], 4,  Web3.toBytes(0x0004), {'from':accounts[1]})
    tokenId = tx.events['TransferSingle']['id']
    with reverts("ERC1155: burn amount exceeds totalSupply"):
        user_collection_1155.burn(accounts[2], tokenId, 5, {"from": accounts[1]})
    user_collection_1155.burn(accounts[2], tokenId, 4, {"from": accounts[1]})
    assert user_collection_1155.balanceOf(accounts[2], tokenId) == 0

def test_second_deploy_and_mint(accounts, factorySBT, implSBT1155, UsersSBTCollection1155BehindProxy):
    tx = factorySBT.deployProxyFor(
        implSBT1155.address, 
        accounts[2],
        "Second 1155",
        "S1155",
        baseUrl,
        accounts[1]  #wrapper
        ,{'from':accounts[0]}
    )
    logging.info('Result from tx:{}'.format(tx.return_value)) 
    user_collection_1155_next = UsersSBTCollection1155BehindProxy.at(tx.new_contracts[0])
    assert tx.return_value  == user_collection_1155_next.address
    with reverts('Trusted address only'):
        user_collection_1155_next.mintWithRules(accounts[2], 4, Web3.toBytes(0x0001), {'from':accounts[0]})

    #with tokenURI
    tx = user_collection_1155_next.mintWithRules(accounts[2], 4, Web3.toBytes(0x0001), {'from':accounts[1]})    
    tokenId = tx.events['TransferSingle']['id']
    assert user_collection_1155_next.name() == "Second 1155" 
    assert user_collection_1155_next.balanceOf(accounts[2], tokenId) == 4
    assert user_collection_1155_next.totalSupply(tokenId) == 4

    assert user_collection_1155_next.exists(tokenId) == True
        
def test_deploy_wrong(accounts, factorySBT, implSBT1155, UsersSBTCollection1155BehindProxy, erc20):

    #try to call with wrong logic smart contract address
    tx = factorySBT.deployProxyFor(
        accounts[1],
        accounts[1],
        "Wrong 1155",
        "W1155",
        baseUrl,
        accounts[1]  #wrapper
        ,{'from':accounts[0]}
    )  

    #try to call with wrong logic smart contract address - logic smart contract has revert in initialize method
    with reverts("Construction failed"):
        tx = factorySBT.deployProxyFor(
            erc20,
            accounts[1],
            "Wrong erc20",
            "Werc20",
            baseUrl,
            accounts[1]  #wrapper
            ,{'from':accounts[0]}
        ) 

def test_factorySBT_mint_mock(accounts, factorySBT, implSBT1155, UsersSBTCollection1155BehindProxy, mockWrapper):  

    tx = factorySBT.deployProxyFor(
        implSBT1155.address, 
        accounts[1],
        "First 1155",
        "wF1155",
        baseUrl,
        mockWrapper #wrapper
        ,{'from':accounts[0]}
    )
    proxy_address = tx.new_contracts[0]

    user_collection_1155_1 = UsersSBTCollection1155BehindProxy.at(tx.new_contracts[0])

    assert user_collection_1155_1.wnftInfo(0)[6] == '0x0005'

    
    
    
    


