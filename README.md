# Envelop Mint App - User's NFT factory

## Brownie Development
We use Brownie framework for developing and unit test. For run tests
first please [install it](https://eth-brownie.readthedocs.io/en/stable/install.html)  
To run long tests you must rename test files in tests folder before running (delete "long_").

So just clone https://github.com/dao-envelop/subscription , install dependencies and good luck!


### Dependencies
Please look at [brownie-config.yaml](./brownie-config.yaml)



## Foundry Development

**Foundry is a blazing fast, portable and modular toolkit for Ethereum application development written in Rust.**

Foundry consists of:

-   **Forge**: Ethereum testing framework (like Truffle, Hardhat and DappTools).
-   **Cast**: Swiss army knife for interacting with EVM smart contracts, sending transactions and getting chain data.
-   **Anvil**: Local Ethereum node, akin to Ganache, Hardhat Network.
-   **Chisel**: Fast, utilitarian, and verbose solidity REPL.

## Documentation

https://book.getfoundry.sh/

## Usage

### Build

```shell
$ forge build
```

### Test

```shell
$ forge test
```

### Format

```shell
$ forge fmt
```

### Gas Snapshots

```shell
$ forge snapshot
```

### Anvil

```shell
$ anvil
```

## Deploy 
### Blast Sepolia
```shell
$ forge script script/Deploy.s.sol:DeployScript --rpc-url blast_sepolia  --account ttwo --sender 0xDDA2F2E159d2Ce413Bd0e1dF5988Ee7A803432E3 --broadcast
```

### Arbitrum (forge create)
```shell
$ forge script script/DeploySBT.s.sol:DeploySbt --rpc-url arbitrum  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B   --etherscan-api-key $ARBISCAN_TOKEN  --broadcast  --verify

```
```shell
# Arbitrum 721 implementation, verified OK
$ forge  create --rpc-url arbitrum  --account envdeployer   --etherscan-api-key $ARBISCAN_TOKEN  --verify  contracts/PublicUsersCollection721BehindProxy.sol:PublicUsersCollection721BehindProxy

$ cast  send 0x6A148bFA97ecF9714CA049bfBFd7Ccb7cdb1C7fF  "addImplementation((uint8,address))" "(3,0x2E2F00Dfac24C4cCB9c7cCACacFc066bAa2938f5)" --rpc-url arbitrum  --account envdeployer
```

```shell
# incase below verifaction does not work
$ forge  create --rpc-url blast_sepolia  --account ttwo      --constructor-args 0x7E3AD5abA96578f50e2737689c9B457DF8eB8403  --etherscan-api-key "verifyContract"   --verify contracts/UserCollectionRegistry.sol:UserCollectionPRegistry
```

### Verify
```shell
$ forge verify-contract <deployed address>  ./contracts/UserCollectionRegistry.sol:UserCollectionPRegistry --verifier-url 'https://api.routescan.io/v2/network/testnet/evm/168587773/etherscan' --etherscan-api-key "verifyContract" --num-of-optimizations 200 --compiler-version 0.8.21 --constructor-args $(cast abi-encode "constructor(address param1)" 0x7E3AD5abA96578f50e2737689c9B457DF8eB8403)

$ forge verify-contract <deployed address>  ./contracts/UsersCollectionFactory.sol:UsersCollectionFactory --verifier-url 'https://api.routescan.io/v2/network/testnet/evm/168587773/etherscan' --etherscan-api-key "verifyContract" --num-of-optimizations 200 --compiler-version 0.8.21 

$ forge verify-contract <deployed address>  ./contracts/PublicUsersCollection1155BehindProxy.sol:PublicUsersCollection1155BehindProxy --verifier-url 'https://api.routescan.io/v2/network/testnet/evm/168587773/etherscan' --etherscan-api-key "verifyContract" --num-of-optimizations 200 --compiler-version 0.8.21 

$ forge verify-contract <deployed address>  ./contracts/PublicUsersCollection721BehindProxy.sol:PublicUsersCollection721BehindProxy --verifier-url 'https://api.routescan.io/v2/network/testnet/evm/168587773/etherscan' --etherscan-api-key "verifyContract" --num-of-optimizations 200 --compiler-version 0.8.21 

```
### Blast Mainnet
```shell
$ forge script script/Deploy.s.sol:DeployScript --rpc-url blast_mainnet  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast  --verify

$ forge script script/DeploySBT.s.sol:DeploySbt --rpc-url blast_mainnet  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B  --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --priority-gas-price 300000 --broadcast  --verify
```
#### Verify
```shell
$ forge verify-contract 0xA5F11D60d96370878140Fba8783d705C41BDe3BE  ./contracts/PublicUsersCollection721BehindProxy.sol:PublicUsersCollection721BehindProxy --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21 

$ forge verify-contract 0xDA9A85BA4790813332a511a048b5c5D31040D438  ./contracts/PublicUsersCollection1155BehindProxy.sol:PublicUsersCollection1155BehindProxy --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21 

$ forge verify-contract 0xB1DCD0333526A7FCCe4E045360E13Db9b2584Dda  ./contracts/UsersCollectionFactory.sol:UsersCollectionFactory --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21 

$ forge verify-contract 0x446EC4c1793B664Bb4f42b956DdD147362Ef4AB4  ./contracts/UserCollectionRegistry.sol:UserCollectionPRegistry --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21 --constructor-args $(cast abi-encode "constructor(address param1)" 0x68247DF83d594af6332bF901a5fF8c3448622774)
```
### Poligon SBT Factory contracts set (aka EvenManager) 2024-05-26
```shell
$ forge script script/DeploySBT.s.sol:DeploySbt --rpc-url polygon  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B   --etherscan-api-key $POLYGONSCAN_TOKEN  --broadcast  --verify

```

### Cast
#### Blast Sepolia
```shell
## Latest block number
$ cast block --rpc-url blast_sepolia | grep number

## Owner()
$ cast call 0x73a3a59c1C36FA1DBBf799AA719c69a1B82f9A6C "owner()" --rpc-url blast_sepolia

$ cast send 0x73a3a59c1C36FA1DBBf799AA719c69a1B82f9A6C "setSubscriptionOnOff(bool)" false --rpc-url blast_sepolia --account ttwo 

$ cast send 0x73a3a59c1C36FA1DBBf799AA719c69a1B82f9A6C "addImplementation((uint8,address))" "(3,0x0A612A26b5129F43c73F6546a5C1257E81065c3a)" --rpc-url blast_sepolia --account ttwo 

$ cast send 0x73a3a59c1C36FA1DBBf799AA719c69a1B82f9A6C "addImplementation((uint8,address))" "(4,0xaB393AdC3b1F6894d04Cdc782AAbb86B90F8a7c3)" --rpc-url blast_sepolia --account ttwo 

```

### Help

```shell
$ forge --help
$ anvil --help
$ cast --help
```
### Add forge to existing Brownie project
```shell
$ forge init --force
$ forge install OpenZeppelin/openzeppelin-contracts@v4.9.3
$ forge install dao-envelop/envelop-protocol-v1@1.3.1
$ forge install dao-envelop/subscription@2.2.0
$ forge buld
```
### First build
```shell
git clone git@gitlab.com:envelop/mintnft/mintfactory.git
git submodule update --init --recursive
```