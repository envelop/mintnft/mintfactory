from brownie import *
import json

accounts.load('secret2')

def main():
    print('Deployer account= {}'.format(accounts[0]))
    tx_params={'from':accounts[0], 'allow_revert': True, 'gas_limit': 10e6}
    sub_registry = '0x825f886D9116170ba5E8E70132ba72d883C079FE'
    agent = '0xaB393AdC3b1F6894d04Cdc782AAbb86B90F8a7c3'
    niftsy = '0x376e8EA664c2E770E1C45ED423F62495cB63392D'
    PRICE = 1e18

    #impl721 = accounts[0].deploy(PublicUsersCollection721BehindProxy)
    #impl1155 = accounts[0].deploy(PublicUsersCollection1155BehindProxy)
    #factory = accounts[0].deploy(UsersCollectionFactory)
    #registry = accounts[0].deploy(UserCollectionPRegistry, sub_registry)
    

    #PublicUsersCollection721BehindProxy.publish_source(impl721);
    #PublicUsersCollection1155BehindProxy.publish_source(impl1155);
    #UsersCollectionFactory.publish_source(factory);
    #UserCollectionPRegistry.publish_source(registry);

    impl721 = PublicUsersCollection721BehindProxy.at('0x0D0fDf7dF66A771E3480A96F8A5c2768531f930A')
    impl1155 = PublicUsersCollection1155BehindProxy.at('0xA0A62F3F568D1c6595d6399a3929c9C1DA7E9b4E')
    factory = UsersCollectionFactory.at('0x4114Fc9330b420278691490684717238641a7211')
    registry = UserCollectionPRegistry.at('0x8a9230fc057c4c00B19261a1AD1067c6B5082a17')

    
    
    print("----------Deployment artifacts-------------------")
    print("impl721 = PublicUsersCollection721BehindProxy.at('{}')".format(impl721.address))
    print("impl1155 = PublicUsersCollection1155BehindProxy.at('{}')".format(impl1155.address))
    print("factory = UsersCollectionFactory.at('{}')".format(factory.address))
    print("registry = UserCollectionPRegistry.at('{}')".format(registry.address))

    #create tariff
    subscriptionType = (0,0,2,True, accounts[0]) #with count tariff
    payOptions = [(niftsy, PRICE, 0)] #without Agent fee
    tariff1 = (subscriptionType, payOptions)
    registry.registerServiceTariff(tariff1,tx_params)
    registry.authorizeAgentForService(agent, [0],tx_params)

    #make settings
    registry.addImplementation((3, impl721.address),tx_params)
    registry.addImplementation((4, impl1155.address),tx_params)

    registry.setFactory(factory.address, tx_params)
    factory.setOperatorStatus(registry.address, True, tx_params)
    