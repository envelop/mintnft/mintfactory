// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {UsersSBTCollection721BehindProxy}  from "../contracts/UsersSBTCollection721BehindProxy.sol";
import {UsersSBTCollection1155BehindProxy} from "../contracts/UsersSBTCollection1155BehindProxy.sol";
import {UsersSBTCollectionFactory} from "../contracts/UsersSBTCollectionFactory.sol";
import {UsersSBTCollectionRegistry} from "../contracts/UsersSBTCollectionRegistry.sol";
import {EventManager} from "../contracts/cases/EventManager.sol";


contract DeploySbt is Script {
    using stdJson for string;

    function run() public {
        console2.log("Chain id: %s", vm.toString(block.chainid));
        console2.log("Deployer address: %s, %s", msg.sender, msg.sender.balance);

        // Load json with chain params
        string memory root = vm.projectRoot();
        string memory params_path = string.concat(root, "/script/chain_params.json");
        string memory params_json_file = vm.readFile(params_path);
        string memory key;

        // Define constructor params
        address sub_reg;   
        key = string.concat(".", vm.toString(block.chainid),".sub_reg");
        if (vm.keyExists(params_json_file, key)) 
        {
            sub_reg = params_json_file.readAddress(key);
        } else {
            sub_reg = msg.sender;
        }
        console2.log("sub_reg: %s", sub_reg); 
        
        
        
        //////////   Deploy   //////////////
        vm.startBroadcast();
        UsersSBTCollection721BehindProxy impl721  = new UsersSBTCollection721BehindProxy();
        UsersSBTCollection1155BehindProxy impl1155  = new UsersSBTCollection1155BehindProxy();
        UsersSBTCollectionFactory factory   = new UsersSBTCollectionFactory();
        EventManager registry = new EventManager(sub_reg);
        vm.stopBroadcast();
        
        ///////// Pretty printing ////////////////
        
        string memory path = string.concat(root, "/script/explorers.json");
        string memory json = vm.readFile(path);
        console2.log("Chain id: %s", vm.toString(block.chainid));
        string memory explorer_url = json.readString(
            string.concat(".", vm.toString(block.chainid))
        );
        
        console2.log("\n**UsersSBTCollection721BehindProxy**  ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(impl721));
        console2.log("\n**UsersSBTCollection1155BehindProxy**  ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(impl1155));
        console2.log("\n**UsersSBTCollectionFactory** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(factory));
        console2.log("\n**EventManager** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(registry));

        console2.log("```python");
        console2.log("sub_reg = SubscriptionRegistry.at('%s')", address(sub_reg));
        console2.log("impl721 = UsersSBTCollection721BehindProxy.at('%s')", address(impl721));
        console2.log("impl1155 = UsersSBTCollection1155BehindProxy.at('%s')", address(impl1155));
        console2.log("factory = UsersSBTCollectionFactory.at('%s')", address(factory));
        console2.log("registry = EventManager.at('%s')", address(registry));
        console2.log("```");
   
        ///////// End of pretty printing ////////////////

        ///  Init ///
        console2.log("Init transactions....");
        vm.startBroadcast();
        registry.addImplementation(
            UsersSBTCollectionRegistry.Asset(UsersSBTCollectionRegistry.AssetType.ERC721, address(impl721))
        );
        registry.addImplementation(
            UsersSBTCollectionRegistry.Asset(UsersSBTCollectionRegistry.AssetType.ERC1155, address(impl1155))
        );

        registry.setFactory(address(factory));
        factory.setOperatorStatus(address(registry), true);
        registry.setSubscriptionOnOff(false);
                
        vm.stopBroadcast();
        console2.log("Initialisation finished");

    }
}
