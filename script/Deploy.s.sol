// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {PublicUsersCollection721BehindProxy}  from "../contracts/PublicUsersCollection721BehindProxy.sol";
import {PublicUsersCollection1155BehindProxy} from "../contracts/PublicUsersCollection1155BehindProxy.sol";
import {UsersCollectionFactory} from "../contracts/UsersCollectionFactory.sol";
import {UserCollectionPRegistry} from "../contracts/UserCollectionRegistry.sol";


contract DeployScript is Script {
    using stdJson for string;

    function run() public {
        console2.log("Chain id: %s", vm.toString(block.chainid));
        console2.log("Deployer address: %s, %s", msg.sender, msg.sender.balance);

        // Load json with chain params
        string memory root = vm.projectRoot();
        string memory params_path = string.concat(root, "/script/chain_params.json");
        string memory params_json_file = vm.readFile(params_path);
        string memory key;

        // Define constructor params
        address sub_reg;   
        key = string.concat(".", vm.toString(block.chainid),".sub_reg");
        if (vm.keyExists(params_json_file, key)) 
        {
            sub_reg = params_json_file.readAddress(key);
        } else {
            sub_reg = msg.sender;
        }
        console2.log("sub_reg: %s", sub_reg); 
        
        
        
        //////////   Deploy   //////////////
        vm.startBroadcast();
        PublicUsersCollection721BehindProxy impl721  = new PublicUsersCollection721BehindProxy();
        PublicUsersCollection1155BehindProxy impl1155 = new PublicUsersCollection1155BehindProxy();
        UsersCollectionFactory factory   = new UsersCollectionFactory();
        UserCollectionPRegistry registry = new UserCollectionPRegistry(sub_reg);
        vm.stopBroadcast();
        
        ///////// Pretty printing ////////////////
        
        string memory path = string.concat(root, "/script/explorers.json");
        string memory json = vm.readFile(path);
        console2.log("Chain id: %s", vm.toString(block.chainid));
        string memory explorer_url = json.readString(
            string.concat(".", vm.toString(block.chainid))
        );
        
        console2.log("\n**PublicUsersCollection721BehindProxy**  ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(impl721));
        console2.log("\n**PublicUsersCollection1155BehindProxy** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(impl1155));
        console2.log("\n**UsersCollectionFactory** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(factory));
        console2.log("\n**UserCollectionPRegistry** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(registry));

        console2.log("```python");
        console2.log("sub_reg = SubscriptionRegistry.at('%s')", address(sub_reg));
        console2.log("impl721 = PublicUsersCollection721BehindProxy.at('%s')", address(impl721));
        console2.log("impl1155 = PublicUsersCollection1155BehindProxy.at('%s')", address(impl1155));
        console2.log("factory = UsersCollectionFactory.at('%s')", address(factory));
        console2.log("registry = UserCollectionPRegistry.at('%s')", address(registry));
        console2.log("```");
   
        ///////// End of pretty printing ////////////////

        ///  Init ///
        console2.log("Init transactions....");
        vm.startBroadcast();
        registry.addImplementation(
            UserCollectionPRegistry.Asset(UserCollectionPRegistry.AssetType.ERC721, address(impl721))
        );
        registry.addImplementation(
            UserCollectionPRegistry.Asset(UserCollectionPRegistry.AssetType.ERC1155, address(impl1155))
        );

        registry.setFactory(address(factory));
        factory.setOperatorStatus(address(registry), true);
        registry.setSubscriptionOnOff(false);
                
        vm.stopBroadcast();
        console2.log("Initialisation finished");

    }
}
